module First where

a :: String
a = "{\"0\": {\"x\":   0,  \"y\":   0, \"v\":  \"x\"},   \"1\": {\"x\":  1,   \"y\":  0, \"v\": \"o\"}, \"2\":   {\"x\":  1,   \"y\": 0,   \"v\":  \"x\"}}"

type Move = (Int, Int, Char)
type Moves = [Move]

validate :: String -> Bool
validate [] = True
validate input = 
  let
    moves = parse input
    validate = all(==False)[not(sameMovesCount moves), existsOverlappingMoves moves, not(movesInSequence moves)]
  in
    validate

sameMovesCount :: Moves -> Bool
sameMovesCount [] = False
sameMovesCount moves =
  let
    movesCountX = countScore moves 'x'
    movesCountO = countScore moves 'o'
  in
    movesCountX == movesCountO

existsOverlappingMoves :: Moves -> Bool
existsOverlappingMoves [] = False
existsOverlappingMoves moves =
  let
    overlaps = 
      (doCellOverlaps moves 0 0) ||
      (doCellOverlaps moves 0 1) ||
      (doCellOverlaps moves 0 2) ||
      (doCellOverlaps moves 1 0) ||
      (doCellOverlaps moves 1 1) ||
      (doCellOverlaps moves 1 2) ||
      (doCellOverlaps moves 2 0) ||
      (doCellOverlaps moves 2 1) ||
      (doCellOverlaps moves 2 2)
  in
    overlaps

trim :: [Char] -> [Char]
trim [] = []
trim (' ':xs) = trim xs
trim (x:xs) =
  let
    rest = trim xs
  in
    x:rest

parse :: String -> Moves
parse ('{':rest) = parseMoves (trim rest)
parse _ = error "Ne sarasas"

parseMoves :: String -> Moves
parseMoves "}" = []
parseMoves ('"' :key: '"' : ':' :rest) =
  let
    (move, restNow) = parseMove rest
    answ = parseMoves restNow
  in
    move:answ
parseMoves (',' :rest) = parseMoves rest
parseMoves _ = error "Invalid data"

parseMove :: String -> (Move, String)
parseMove ('{' : '"' : 'x' : '"' : ':' :x: ',' : '"' : 'y' : '"' : ':' :y: ',' : '"' : 'v' : '"' : ':' : '"' :v: '"' : '}' :rest) = ((toDigit x, toDigit y, v), rest)
parseMove _ = error "Not valid move"

toDigit :: Char -> Int
toDigit '0' = 0
toDigit '1' = 1
toDigit '2' = 2
toDigit _ = error "Such coordinate doesn't exist"

countScore :: Moves -> Char -> Int
countScore moves p = length $ filter (\(x, y, player) -> player == p) moves

doCellOverlaps :: Moves -> Int -> Int -> Bool
doCellOverlaps [] x y = False
doCellOverlaps moves x y =
  let
    count = length $ filter (\(xc, yc, player) -> (xc == x) && (yc == y)) moves
  in
    count > 1

getPlayer :: Move -> Char
getPlayer move =
  let
    moveArr = [move]
    count = length $ filter (\(xc, yc, player) -> (player == 'x')) moveArr
  in
    if count > 0
      then 'x'
      else 'o'

samePlayers :: Move -> Move -> Bool
samePlayers move1 move2 =
  let
    player1 = getPlayer move1
    player2 = getPlayer move2
  in
    player1 == player2

movesInSequence :: Moves -> Bool
movesInSequence [] = True
movesInSequence moves =
  let
    inSequence = if (length moves > 1) && not(sameFirstTwoPlayers moves)
      then movesInSequence (tail moves)
      else if (length moves < 2)
        then True
        else False
  in
    inSequence

sameFirstTwoPlayers :: Moves -> Bool
sameFirstTwoPlayers moves =
  let
    move1 = head moves
    move2 = head (tail moves)
  in
    samePlayers move1 move2